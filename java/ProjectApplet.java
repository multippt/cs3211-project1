import java.awt.*;
import java.awt.event.*;
import javax.swing.*; 

public class ProjectApplet extends JPanel implements ActionListener {
	protected JTextField textField;
    protected JTextArea textArea;
	protected JTextArea textArea2;
    private final static String newline = "\n";
	private MainSystem projectSystem;
	private User user;

    public ProjectApplet() {
        super(new GridBagLayout());

		// Command input
        textField = new JTextField(40);
        textField.addActionListener(this);

		// Response display
        textArea = new JTextArea(10, 40);
        textArea.setEditable(false);
        JScrollPane scrollPane = new JScrollPane(textArea);
		
		// Log display
		textArea2 = new JTextArea(10, 40);
        textArea2.setEditable(false);
        JScrollPane scrollPane2 = new JScrollPane(textArea2);

		// Add to GUI
        GridBagConstraints c = new GridBagConstraints();
        c.gridwidth = GridBagConstraints.REMAINDER;
        c.fill = GridBagConstraints.HORIZONTAL;
        add(textField, c);
        c.fill = GridBagConstraints.BOTH;
        c.weightx = 1.0;
        c.weighty = 1.0;
        add(scrollPane, c);
		add(scrollPane2, c);
		
		// Run project system
		MainSystem.showConsole = false;
		MainSystem.logListener = this;
		MainSystem.main(new String[0]);
		user = new User();
    }

	// Triggered when an event occurs for the GUI
    public void actionPerformed(ActionEvent evt) {
		if (evt.getActionCommand().equals("log")) { // Log event
			textArea2.setText(MainSystem.getLog());
			textArea2.setCaretPosition(textArea2.getDocument().getLength()); // Skip to end of textarea
		} else if (evt.getActionCommand().equals("userlog")) { // Response event
			textArea.setText(MainSystem.getUserLog());
			textArea.setCaretPosition(textArea.getDocument().getLength()); // Skip to end of textarea
		} else { // Text input event
			String text = textField.getText();
			textField.setText("");
			user.sendCommand(text);
		}
    }

    private static void createAndShowGUI() {
        //Create and set up the window.
        JFrame frame = new JFrame("CS3211 Project 1 Group 1");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Add contents to the window.
        frame.add(new ProjectApplet());

        //Display the window.
        frame.pack();
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        // Display GUI
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }
}