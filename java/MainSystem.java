import java.util.*;
import java.awt.event.*;

// "System()" process
class MainSystem {
	// Configuration
	public static int ConcentratorCount = 2; // Amount of concentrator
	public static int DCUCount = 1; // Amount of DCU
	public static int UserCount = 1; // Amount of additional Users
	public static int step = 30; // How many milliseconds to wait between each step
	public static boolean enableReorder = true; // Enables the Re-arranger code that fixes out-of-order and duplicates
	public static boolean enableSpontaneous = true; // DCU automatically generates records (if set to "false", use "generate" command to make DCU create records)
	public static boolean showConsole = true; // Using MainSystem from command line. If set to false, please set "logListener" to watch for logs
	
	public static Cloud cloud;
	public static ArrayList<Concentrator> concentrators = new ArrayList<Concentrator>();
	public static ArrayList<DCU> dcus = new ArrayList<DCU>();
	public static ArrayList<User> users = new ArrayList<User>();
	private static String logs = "";
	private static String userlogs = "";
	public static ActionListener logListener = null;
	private static MainSystem system;
	
	public static void main(String[] args) {
		system = new MainSystem();
		system.run();
	}
	
	private void run() {
		ArrayList<Runnable> threadQueue = new ArrayList<Runnable>();
		
		// Create object instance
		MainSystem.cloud = new Cloud();
		threadQueue.add(MainSystem.cloud);
		for (int i = 0; i < ConcentratorCount; i++) {
			Concentrator newConc = new Concentrator("conc" + i);
			MainSystem.concentrators.add(newConc);
			threadQueue.add(newConc);
		}
		for (int i = 0; i < DCUCount; i++) {
			DCU newDCU = new DCU("dcu" + i);
			MainSystem.dcus.add(newDCU);
			threadQueue.add(newDCU);
		}
		
		// Run all threads
		for (int i = 0; i < threadQueue.size(); i++) {
			Thread t = new Thread(threadQueue.get(i));
			t.start();
		}
		
		// User threads
		for (int i = 0; i < UserCount; i++) {
			User newUser = new User();
			MainSystem.users.add(newUser);
			Thread userThread = new Thread(newUser);
			userThread.start();
		}
		
		// Manual user (this will block the current thread)
		MainSystem.Log("Available commands: start, stop, list, listconfig, quit", 1);
		if (MainSystem.showConsole) {
			User u = new User();
			u.manualRun();
		}
	}
	
	/** GUI/Log-related functions **/
	
	// Message logging
	public static void Log(String message) {
		MainSystem.Log(message, 0);
	}
	public static void Log(String message, int level) {
		if (MainSystem.showConsole) {
			System.out.println(message);
		} else {
			if (MainSystem.logListener != null) {
				if (level == 1) { // User generated messages (responses to commands)
					MainSystem.userlogs += message + "\n";
					MainSystem.logListener.actionPerformed(new ActionEvent(system, 0, "userlog"));
				} else { // Other logs
					MainSystem.logs += message + "\n";
					MainSystem.logListener.actionPerformed(new ActionEvent(system, 0, "log"));
				}
			}
		}
	}
	
	// Get logs for GUI
	public static String getLog() {
		return MainSystem.logs;
	}
	public static String getUserLog() {
		return MainSystem.userlogs;
	}
	
	/** System commands **/
	
	// Runs a system command. If empty response means no system command used.
	public static String RunCommand(String command) {
		String response = "";
		switch (command) {
			case "list": // List records in cloud
				response = MainSystem.listRecords(false);
				break;
			case "listrecord": // List records in all places
				response = MainSystem.listRecords(true);
				break;
			case "listconfig":
				response = MainSystem.listConfigs();
				break;
			case "orderon":
				MainSystem.enableReorder = true;
				MainSystem.reset();
				response = "Rearranger enabled, system reset to allow re-ordering to work";
				break;
			case "orderoff":
				MainSystem.enableReorder = false;
				response = "Rearranger disabled";
				break;
			case "debugon":
				UnreliableNetwork.debug = true;
				response = "Network logging enabled";
				break;
			case "debugoff":
				UnreliableNetwork.debug = false;
				response = "Network logging disabled";
				break;	
			case "useron":
				MainSystem.enableUsers(true);
				response = "Automated user activity enabled";
				break;
			case "useroff":
				MainSystem.enableUsers(false);
				response = "Automated user activity disabled";
				break;
			case "clear": // Reset system
				MainSystem.reset();
				response = "Cleared system records";
				break;
			case "flood": // Put a record in each concentrator to make it easier to trigger out-of-order
				MainSystem.flood();
				response = "Added records to all concentrators";
				break;
			case "quit":
				MainSystem.Log("Exiting system");
				System.exit(0);
				break;
		}
		return response;
	}
	
	// Generates a list of records received in chronological order
	private static String listRecords(boolean verbose) {
		String result = "";
		if (verbose) {
			result += "Cloud:\n";
		}
		for (int i = 0; i < MainSystem.cloud.records.size(); i++) {
			result += "* " + MainSystem.cloud.records.get(i) + "\n";
		}
		if (verbose) {
			// Also get record information from other places
			for (int i = 0; i < MainSystem.dcus.size(); i++) {
				result += "DCU" + i + ": ";
				DCU dcu = MainSystem.dcus.get(i);
				for (int j = 0; j < dcu.records.size(); j++) {
					if (j > 0) {
						result += ",";
					}
					result += dcu.records.get(j);
				}
				result += "\n";
			}
			for (int i = 0; i < MainSystem.concentrators.size(); i++) {
				result += "CONC" + i + ": ";
				Concentrator c = MainSystem.concentrators.get(i);
				for (int j = 0; j < c.records.size(); j++) {
					if (j > 0) {
						result += ",";
					}
					result += c.records.get(j);
				}
				result += "\n";
			}
		}
		return result;
	}
	
	// Generates a list of configurations for DCUs on all locations
	private static String listConfigs() {
		String result = "";
		result += "Cloud: ";
		for (int i = 0; i < MainSystem.dcus.size(); i++) {
			if (i > 0) {
				result += ",";
			}
			result += MainSystem.cloud.dcuconfig.get("dcu" + i);
		}
		
		for (int i = 0; i < MainSystem.dcus.size(); i++) {
			result += "\nDCU" + i + ": " + MainSystem.dcus.get(i).config;
		}
		
		for (int i = 0; i < MainSystem.concentrators.size(); i++) {
			result += "\nCONC" + i + ": ";
			for (int j = 0; j < MainSystem.dcus.size(); j++) {
				if (j > 0) {
					result += ",";
				}
				result += MainSystem.concentrators.get(i).dcuConfig.get(j);
			}
		}
		return result;
	}
	
	// Remove records from all components
	private static void reset() {
		MainSystem.cloud.reset();
		for (int i = 0; i < MainSystem.dcus.size(); i++) {
			MainSystem.dcus.get(i).reset();
		}
		for (int i = 0; i < MainSystem.concentrators.size(); i++) {
			MainSystem.concentrators.get(i).reset();
		}
	}
	
	// Add test records to concentrators
	private static void flood() {
		MainSystem.reset();
		for (int i = 0; i < MainSystem.concentrators.size(); i++) {
			MainSystem.concentrators.get(i).addRecord("dcu0,Record_" + i);
		}
	}
	
	// Enable/Disable users
	private static void enableUsers(boolean enable) {
		for (int i = 0; i < MainSystem.users.size(); i++) {
			MainSystem.users.get(i).enabled = enable;
		}
	}
}

// User process
class User implements Runnable {
	public boolean enabled = true; // Enables the automated user
	private static int usercounter = 0;
	private String name;
	
	public User() {
		name = "User" + usercounter;
		usercounter++;
	}
	
	public void run() {
		while (true) {
			// Make user sleep for a while
			for (int i = 0; i < 500; i++) {
				sleep();
			}
			if (this.enabled) {
				MainSystem.Log(this.name + " is performing an action");
				processUserCommand("startdcu" + (MainSystem.DCUCount - 1));
			}
		}
	}
	
	void sleep() {
		try {
			Thread.sleep(MainSystem.step);
		} catch (Exception ex) {
			
		}
	}
	
	// For automated users
	void processUserCommand(String command) {
		String response = MainSystem.cloud.configure(command);
		MainSystem.Log(response);
	}
	
	// For manual users (i.e. non-automated user) running in command line
	public void manualRun() {
		Scanner sc = new Scanner(System.in);
		while (true) {
			try {
				sendCommand(sc.nextLine());
			} catch (Exception ex) {
				MainSystem.Log(ex.getMessage());
				break;
			}
		}
	}
	
	// Send command or configurations to system for manual users
	public void sendCommand(String command) {
		String response = MainSystem.RunCommand(command);
		if (response.equals("")) {
			// Configuration commands to cloud
			response = MainSystem.cloud.configure(command);
		}
		MainSystem.Log(response, 1);
	}
}

// Cloud process
class Cloud implements Runnable, Communicator {
	private int resendThreshold = 50; // Number of steps to wait before resending last transaction
	
	public ArrayList<String> records; // Record cache
	public TreeMap<String,String> dcuconfig; // Keep track of current dcu config [DCU Name, Configuration]
	private LinkedList<Pair<Communicator, String>> inputQueue; // Queue incoming requests for later processing
	private TreeMap<String,Integer> expectedRecords; // Keep track of expected record from each DCU
	private String activeDCU; // Which DCU is being configured by user
	private String state = "ready";
	private int resendCounter = 0;
	
	public Cloud() {
		records = new ArrayList<String>();
		inputQueue = new LinkedList<Pair<Communicator, String>>();
		dcuconfig = new TreeMap<String, String>();
		expectedRecords = new TreeMap<String, Integer>();
		activeDCU = "dcu0";
		
		// Setup configuration info for each DCU
		for (int i = 0; i < MainSystem.DCUCount; i++) {
			dcuconfig.put("dcu" + i, "null");
			expectedRecords.put("dcu" + i, 0);
		}
	}
	
	public void run() {
		while (true) {
			processQueue();
			performTask();
			sleep();
		}
	}
	
	private void sleep() {
		try {
			Thread.sleep(MainSystem.step);
		} catch (Exception ex) {
		}
	}
	
	public void reset() {
		this.expectedRecords.put("dcu0", 0);
		this.records.clear();
	}
	
	private String getConfigurations() {
		String response = "";
		int count = 0;
		for(Map.Entry<String,String> entry : dcuconfig.entrySet()) {
			if (count > 0) {
				response += ",";
			}
			response += entry.getValue();
			count++;
		}
		return response;
	}
	
	// Process received messages
	private void processQueue() {
		if (inputQueue.size() == 0) { // No items
			return;
		}
		Pair<Communicator, String> input = inputQueue.poll();
		Communicator source = input.first;
		String data = input.second;
		
		String[] parts = data.split("\\|");
		String command = parts[1];
		String value = parts[2];
		
		if (this.state.equals("ready")) { // Waiting for any incoming connection
			if (command.equals("config")) { // CloudConnectCONCPhase1
				// Phase 1. Concentrator established connection, reply with configurations
				UnreliableNetwork.Send(this, source, parts[0] + "|configack|" + this.getConfigurations()); // CloudConnectCONCPhase1a
				this.setState("wait");
				return;
			}
		}
		if (this.state.equals("wait")) { // Phase 2. Already connected, waiting for any incoming messages
			// Record upload from concentrator (concrecord)
			if (command.equals("record")) { // CloudConnectCONCPhase2
				// We now know that concentrator had received config so can remove it
				for (int i = 0; i < MainSystem.DCUCount; i++) {
					dcuconfig.put("dcu" + i, "null");
				}
				String response = "";
				if (MainSystem.enableReorder) {
					// Only accept records that match the expected record number
					response = this.arrangeRecord(data);
				} else {
					// Arranger disabled
					response = this.addRecord(data);
				}
				if (!response.equals("")) { // The ACK is not dropped deliberately
					UnreliableNetwork.Send(this, source, response);
				}
				resendCounter = 0;
				return;
			}
			// Concentrator asks to close connection
			if (command.equals("stop")) {
				this.setState("ready");
				return;
			}
		}
	}
	
	private void setState(String state) {
		this.state = state;
	}
	
	private void performTask() {
		switch (this.state) {
			case "ready": // Cloud can get request from any concentrator
				break;
			case "wait": // Cloud is currently connected to a concentrator
				resendCounter++;
				if (resendCounter == resendThreshold) {
					// Connection terminated due to timeout
					resendCounter = 0;
					this.setState("ready");
				}
				break;
		}
	}
	
	// Check if match a command
	private boolean matchCommand(String command, String targetCommand) {
		return (command.length() >= targetCommand.length() && command.substring(0, targetCommand.length()).equals(targetCommand));
	}
	
	// Gets the target DCU for this command. Assumes command already matched
	private String getCommandDCU(String command, String targetCommand) {
		String target = activeDCU;
		if (command.length() > targetCommand.length()) {
			target = command.substring(targetCommand.length());
		}
		return target;
	}
	
	// User configuration
	// CloudConnectUser
	public String configure(String command) {
		String response = "";
		if (matchCommand(command, "start")) { // generateconfig action
			String target = this.getCommandDCU(command, "start");
			response = "Configuration \"start\" generated for " + target;
			this.dcuconfig.put(target, "start");
		}
		if (matchCommand(command, "stop")) { // generateconfig action
			String target = this.getCommandDCU(command, "stop");
			response = "Configuration \"stop\" generated for " + target;
			this.dcuconfig.put(target, "stop");
		}
		if (matchCommand(command, "set")) { // Handle setting of active DCU
			activeDCU = this.getCommandDCU(command, "set");
			response = "Setting active DCU: " + activeDCU;
		}
		return response;
	}
	
	// Arranger
	// Reject records that are out-of-order (i.e. not equal to expected record), thus making the overall record ordering correct
	private String arrangeRecord(String request) {
		String[] parts = request.split("\\|");
		String record = parts[2];
		String[] recordParts1 = record.split(",");
		String dcu = recordParts1[0];
		
		int expectedRecord = expectedRecords.get(dcu);
		String[] recordParts2 = record.split("_");
		int recordNumber = Integer.parseInt(recordParts2[1]);
		if (expectedRecord == recordNumber) {
			// Record number = Expected record, accept it and put into record list (fixes out-of-order)
			expectedRecord++;
			expectedRecords.put(dcu, expectedRecord);
			return this.addRecord(request);
		} else {
			MainSystem.Log("Dropped record " + recordNumber + " as out-of-order");
			if (expectedRecord > recordNumber) {
				// Record number of earlier record, acknowledge but do not add it to record list (fixes duplicate)
				return parts[0] + "|ack|" + parts[2];
			}
		}
		// Newer record, but out-of-order, don't ACK so concentrators can resend later
		return "";
	}
	
	// Process message and add record to system
	private String addRecord(String request) {
		String[] parts = request.split("\\|");
		this.records.add(parts[2]);
		MainSystem.Log("Cloud received: " + parts[2]);
		return parts[0] + "|ack|" + parts[2];
	}
	
	// Called when received data
	public synchronized void OnReceive(Communicator source, String data) {
		inputQueue.add(new Pair<Communicator, String>(source, data));
	}
}

// DCU process
class DCU implements Runnable, Communicator {
	// Configuration
	private int resendThreshold = 50; // Number of steps to wait before resending last transaction
	private int dataThreshold = 1; // If number of records fall below this, new records will be made
	private int recordsToMake = 3; // Number of records to spontaneously make if DCU detects it have enough space
	
	public LinkedList<String> records; // Pending records
	private String state = "ready"; // Current state
	private Communicator connectedConcentrator = null; // Concentrator to resend to
	private LinkedList<Pair<Communicator, String>> inputQueue; // Pending transactions/ACKs from concentrator
	private int recordCounter; // For labelling records
	
	private int resendCounter = 0;
	private int transactionCounter = 0;
	private String name = "";
	private Random random = new Random(0);
	private int chooseConfigOrDataGeneration = 0;
	public String config = "null";
	
	public DCU(String name) {
		this.name = name;
		records = new LinkedList<String>();
		inputQueue = new LinkedList<Pair<Communicator, String>>();
		recordCounter = 0;
	}
	
	// Creates a unique transaction number
	private String generateTransactionId() {
		return name + "_" + transactionCounter++;
	}
	
	// Generates the message to be sent
	private String generateMessage(String action) {
		String message = generateTransactionId() + "|";
		switch (action) {
			case "config":
				message += "config|" + name;
				break;
			case "stop":
				message += "stop|" + name;
				break;
			case "record":
				message += "record|" + this.records.get(0);
				break;
		}
		return message;
	}
	
	// Handle the responses from concentrator
	private void processQueue() {
		if (inputQueue.size() == 0) { // No items
			return;
		}
		// Read one message
		Pair<Communicator, String> input = inputQueue.poll();
		Communicator source = input.first;
		String data = input.second;
		
		String[] parts = data.split("\\|");
		String command = parts[1];
		String value = parts[2];
		
		switch (this.state) {
			case "phase1": // Connection establishment response (dcuconfig -> concreplyconfig)
				// Expect a config message
				if (command.equals("configack")) {
					// Config response received, now start sending records
					this.setState("send");
					
					// If value is not "null", then it means there is config (concReplyConfigYes)
					if (!value.equals("null")) {
						this.config = value;
						MainSystem.Log(name + " received configuration \"" + value + "\"");
					}
				}
				break;
			case "phase2": // Record sending (dcusendrecord -> concrecordack)
				// Expect a ACK
				if (command.equals("ack")) {
					// Record successfully received by a concentrator
					records.poll();
					this.setState("send");
				}
				break;
		}
	}
	
	// Perform a task depending on current state
	private void performTask() {
		switch (this.state) {
			case "ready": 
				// If it has records to send, it should try to upload them
				chooseConfigOrDataGeneration = (chooseConfigOrDataGeneration + 1) % 2;
				switch (chooseConfigOrDataGeneration) {
					case 0:
						// We generate record if there is space in DCU (DCUMakeRecord)
						if (this.records.size() < dataThreshold && MainSystem.enableSpontaneous) {
							MainSystem.Log(this.name + " generated record");
							this.setState("generatedata");
							break;
						}
					case 1: // Init a connection if we have records to send (DCUConnect)
						if (this.records.size() > 0) {
							this.setState("connect");
						}
						break;
				}
				break;
			case "connect": // DCUConnectPhase1
				// Attempt to connect to a concentrator and start a connection
				int concentratorChoice = random.nextInt(MainSystem.concentrators.size());
				connectedConcentrator = MainSystem.concentrators.get(concentratorChoice);
				UnreliableNetwork.Send(this, connectedConcentrator, generateMessage("config"));
				this.setState("phase1");
				break;
			case "send": // Phase 2 and 3
				if (this.records.size() > 0) { // DCUConnectPhase2
					// Upload a record
					UnreliableNetwork.Send(this, connectedConcentrator, generateMessage("record"));
					this.setState("phase2");
				} else { // DCUConnectPhase3
					// No more records
					UnreliableNetwork.Send(this, connectedConcentrator, generateMessage("stop"));
					this.setState("ready");
				}
				break;
			case "phase1": 
			case "phase2":
				// Wait for response
				resendCounter++;
				if (resendCounter == resendThreshold) {
					// Connection terminated due to timeout, DCU should try to reconnect later
					resendCounter = 0 ;
					this.setState("ready");
				}
				break;
			case "generatedata": // DCUMakeRecord
				// Creates a few new records
				for (int i = 0; i < this.recordsToMake; i++) {
					this.records.add(this.name + ",Record_" + recordCounter++);
				}
				this.setState("ready");
				break;
		}
	}
	
	public void run() {
		while (true) {
			processQueue();
			performTask();
			sleep();
		}
	}
	
	private void sleep() {
		try {
			Thread.sleep(MainSystem.step);
		} catch (Exception ex) {
		}
	}
	
	private void setState(String state) {
		this.state = state;
	}
	
	// For debug
	public void reset() {
		this.recordCounter = 0;
		this.records.clear();
	}
	
	// Concentrator -> DCU: Called when concentrator responds
	public synchronized void OnReceive(Communicator source, String data) {
		inputQueue.add(new Pair<Communicator, String>(source, data));
	}
}

// Concentrator process
class Concentrator implements Runnable, Communicator {
	public LinkedList<String> records;
	private String state = "ready";
	private LinkedList<Pair<Communicator, String>> inputQueue; // Queue incoming requests for later processing
	private String name = "";
	private int resendCounter = 0;
	private int resendThreshold = 50;
	private int transactionCounter = 0;
	public ArrayList<String> dcuConfig; // List of pending DCU configurations
	String activeDCU = ""; // The currently connected DCU (if currently active connection)
	
	public Concentrator(String name) {
		this.name = name;
		records = new LinkedList<String>();
		inputQueue = new LinkedList<Pair<Communicator, String>>();
		dcuConfig = new ArrayList<String>();
		for (int i = 0; i < MainSystem.DCUCount; i++) {
			dcuConfig.add("null");
		}
	}
	
	// Creates a unique transaction number
	String generateTransactionId() {
		return name + "_" + transactionCounter++;
	}
	
	// Start a new transaction (assumes last transaction completed)
	void initiateTransaction(String message) {
		resendCounter = 0 ;
		this.setState("wait");
		UnreliableNetwork.Send(this, MainSystem.cloud, message);
	}
	
	// Generates the message to be sent
	private String generateMessage(String action) {
		String message = generateTransactionId() + "|";
		switch (action) {
			// Communication between DCU and concentrator
			case "configack": // concreplyconfig
				// Reply to DCU configuration
				message += "configack|" + this.dcuConfig.get(this.getDCUNumber(activeDCU));
				break;
			case "ack": // concrecordack
				// Reply to DCU record ACK
				message += "ack|" + this.records.get(this.records.size() - 1);
				break;
			case "stop":
				// Close connection between DCU and concentrator
				message += "stop|" + name;
				break;
			// Communication between concentrator and cloud
			case "config": // concconfig
				// Start connection to cloud
				message += "config|" + name;
				break;
			case "record": // concrecord
				// Upload record to cloud
				message += "record|" + this.records.get(0);
				break;
		}
		return message;
	}
	
	// Process DCU configs received from cloud
	private void processConfig(String config) {
		MainSystem.Log(name + " received configurations: " + config);
		String[] parts = config.split(",");
		for (int i = 0; i < parts.length; i++) {
			if (this.dcuConfig.size() > i) {
				if (!parts[i].equals("null")) {
					this.dcuConfig.set(i, parts[i]);
				}
			}
		}
	}
	
	private int getDCUNumber(String dcuName) {
		String numberPart = dcuName.substring(3, dcuName.length());
		return Integer.parseInt(numberPart);
	}
	
	// Process responses from cloud or requests from DCU
	private Communicator activeCommunicator = null;
	private void processQueue() {
		if (inputQueue.size() == 0) { // No items
			return;
		}
		// Read one message
		Pair<Communicator, String> input = inputQueue.poll();
		Communicator source = input.first;
		String data = input.second;
		
		String[] parts = data.split("\\|");
		String command = parts[1];
		String value = parts[2];
		
		if (this.state.equals("ready")) {
			// Communicator can accept a connection from DCU, respond with configurations
			if (command.equals("config")) { // CONCConnectDCUPhase1
				activeDCU = value;
				activeCommunicator = source;
				UnreliableNetwork.Send(this, activeCommunicator, generateMessage("configack")); // CONCConnectDCUPhase1a
				this.setState("connecteddcu");
				return;
			}
		}
		
		// Current connection is with DCU
		if (this.state.equals("connecteddcu")) {
			// Current connection is between DCU and concentrator
			if (command.equals("record")) { // CONCConnectDCUPhase2
				// Remove any existing configuration for the DCU as now we know it has received it
				this.dcuConfig.set(this.getDCUNumber(this.activeDCU), "null");
				
				// DCU upload a record
				this.records.add(value);
				UnreliableNetwork.Send(this, activeCommunicator, generateMessage("ack"));
				MainSystem.Log(this.name + " received record " + value);
				resendCounter = 0;
			}
			if (command.equals("stop")) {
				// DCU closed the connection
				activeCommunicator = null;
				this.setState("ready");
				resendCounter = 0;
			}
		}
		
		// Current connection is with cloud
		if (this.state.equals("phase1")) {
			if (command.equals("configack")) {
				// Concentrator managed to establish a connection to cloud, and process cloud configurations
				this.setState("upload");
				this.processConfig(value); // CONCConnectCloudPhase1a
			}
		}
		if (this.state.equals("phase2")) {
			if (command.equals("ack")) {
				// ACK from cloud
				this.records.poll();
				this.setState("upload"); // Upload another record
			}
		}
	}
	
	private void performTask() {
		switch (this.state) {
			case "ready": 
				// If it has records to send, it should try to upload them
				if (this.records.size() > 0) {
					this.setState("connect");
				}
				break;
			case "connect": // CONCConnectCloudPhase1
				// Attempt to connect to cloud and start a connection
				UnreliableNetwork.Send(this, MainSystem.cloud, generateMessage("config"));
				this.setState("phase1");
				break;
			case "upload": // CONCConnectCloudPhase2
				if (this.records.size() > 0) {
					// Upload a record
					UnreliableNetwork.Send(this, MainSystem.cloud, generateMessage("record"));
					this.setState("phase2");
				} else {
					// No more records
					UnreliableNetwork.Send(this, MainSystem.cloud, generateMessage("stop"));
					this.setState("ready");
				}
				break;
			case "phase1": // Concentrator <-> Cloud phase 1 (timeout detection)
			case "phase2": // Concentrator <-> Cloud phase 2
			case "connecteddcu": // DCU <-> Concentrator
				// Wait for response
				resendCounter++;
				if (resendCounter == resendThreshold) {
					// Current connection terminated due to timeout
					resendCounter = 0;
					this.setState("ready");
				}
				break;
		}
	}
	
	public void run() {
		while (true) {
			processQueue();
			performTask();
			sleep();
		}
	}
	
	private void sleep() {
		try {
			Thread.sleep(MainSystem.step);
		} catch (Exception ex) {
			
		}
	}
	
	private void setState(String state) {
		this.state = state;
	}
	
	// For debug
	public void reset() {
		this.records.clear();
	}
	
	// For debug
	public void addRecord(String record) {
		this.records.add(record);
	}
	
	// Called when either DCU or Cloud communicates
	public synchronized void OnReceive(Communicator source, String data) {
		inputQueue.add(new Pair<Communicator, String>(source, data));
	}
}

// An interface allows connecting to a network
interface Communicator {
	public void OnReceive(Communicator source, String data); // Send data to recipient
}

// Simulate the network
class UnreliableNetwork {
	private static boolean enableDrop = true; // Enables random dropping
	public static boolean debug = false; // Show debug mesasges
	private static Random random = new Random(0);
	private static int max = 100;
	private static int chance = 80;
	
	// Data can either be dropped (timeout) or received successfully by other end
	public static void Send(Communicator source, Communicator destination, String data) {
		if (debug) {
			MainSystem.Log("Send:" + data);
		}
		
		boolean successSend = (random.nextInt(max) < chance); // Randomly drop transactions (50% drop)
		if (!enableDrop || successSend) {
			destination.OnReceive(source, data);
		} else {
			if (debug) {
				MainSystem.Log("Drop");
			}
		}
	}
}

// Helper class to represent a pair
class Pair<K, V> {
    public K first;
    public V second;

    public Pair(K first, V second) {
        this.first = first;
        this.second = second;
    }
}
